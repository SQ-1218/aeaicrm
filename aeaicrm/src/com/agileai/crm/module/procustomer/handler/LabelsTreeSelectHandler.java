package com.agileai.crm.module.procustomer.handler;

import java.util.List;

import com.agileai.crm.module.procustomer.service.LabelsTreeSelect;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.TreeBuilder;

public class LabelsTreeSelectHandler extends TreeSelectHandler {
	public LabelsTreeSelectHandler() {
		super();
		this.serviceId = buildServiceId(LabelsTreeSelect.class);
		this.isMuilSelect = false;
	}

	protected TreeBuilder provideTreeBuilder(DataParam param) {
    	if(param.get("targetId").equals("ORG_LABELS")){
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "CODE_ID",
                                                  "CODE_NAME", "TYPE_ID");
        String rootId = param.get("ORG_LABELS");
        treeBuilder.setRootId(rootId);
        return treeBuilder;
    	}else if(param.get("targetId").equals("PER_LABELS")){
    	List<DataRow> records = getService().queryPerTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "CODE_ID",
                                                      "CODE_NAME", "TYPE_ID");
        String rootId = param.get("PER_LABELS");
        treeBuilder.setRootId(rootId);
        return treeBuilder;	
    	}
		return null;
    }

	protected LabelsTreeSelect getService() {
		return (LabelsTreeSelect) this.lookupService(this.getServiceId());
	}
}
