package com.agileai.crm.module.procustomer.handler;

import java.util.List;

import com.agileai.crm.cxmodule.ProcustVisitManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class ProcustVisitManageListHandler
        extends StandardListHandler {
    public ProcustVisitManageListHandler() {
        super();
        this.editHandlerClazz = ProcustVisitManageEditHandler.class;
        this.serviceId = buildServiceId(ProcustVisitManage.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		Boolean isClose = param.getBoolean("isClose");
		if(isClose){
			this.setAttribute("isClose", isClose);
		}else{
			this.setAttribute("goBack", true);
		}
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        initMappingItem("PROCUST_VISIT_EFFECT",
                        FormSelectFactory.create("VISIT_EFFECT").getContent());
        initMappingItem("PROCUST_VISIT_TYPE",
                FormSelectFactory.create("VISIT_TYPE").getContent());
        initMappingItem("CUST_VISIT_CATEGORY",
                FormSelectFactory.create("CUST_VISIT_CATEGORY").getContent());        
    }

    protected void initParameters(DataParam param) {
    }
    
	public ViewRenderer doBackAction(DataParam param){
		return new RedirectRenderer(getHandlerURL(OrgInfoManageListHandler.class));
	}

    protected ProcustVisitManage getService() {
        return (ProcustVisitManage) this.lookupService(this.getServiceId());
    }
}
