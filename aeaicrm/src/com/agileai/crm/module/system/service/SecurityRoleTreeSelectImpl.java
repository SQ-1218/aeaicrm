package com.agileai.crm.module.system.service;

import com.agileai.hotweb.bizmoduler.core.TreeSelectServiceImpl;
import com.agileai.crm.cxmodule.SecurityRoleTreeSelect;

public class SecurityRoleTreeSelectImpl
        extends TreeSelectServiceImpl
        implements SecurityRoleTreeSelect {
    public SecurityRoleTreeSelectImpl() {
        super();
    }
}
