package com.agileai.crm.module.mytasks.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.agileai.crm.cxmodule.ColdCallsManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.MasterSubService;
import com.agileai.hotweb.controller.core.MasterSubEditMainHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.RedirectRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.StringUtil;

public class ColdCallsManageEditHandler
        extends MasterSubEditMainHandler {
    public ColdCallsManageEditHandler() {
        super();
        this.listHandlerClass = ColdCallsManageListHandler.class;
        this.serviceId = buildServiceId(ColdCallsManage.class);
        this.baseTablePK = "TASK_ID";
        this.defaultTabId = "_base";
    }
    
	public ViewRenderer prepareDisplay(DataParam param) {
		if(!"00000000-0000-0000-00000000000000000".equals(param.get("TASK_REVIEW_ID"))){
			DataRow dataRow = getService().getTaskReviewRecord(param);
			String taskReviewState = dataRow.getString("TASK_REVIEW_STATE");
			this.setAttribute("TASK_REVIEW_STATE", taskReviewState);
		}
		
		String operaType = param.get(OperaType.KEY);
		if (isReqRecordOperaType(operaType)){
			DataRow record = getService().getMasterRecord(param);
			this.setAttributes(record);
			String custId = record.getString("CUST_ID");
			String orgName = record.getString("ORG_NAME");
			if(!StringUtil.isNotNullNotEmpty(custId)){
				DataRow dataRow = getService().getCustIdInfoRecord(new DataParam("orgName", orgName));
				if(dataRow != null && !dataRow.isEmpty()){
					this.setAttribute("CUST_ID", dataRow.getString("CUST_ID"));
				}
			}
		}
		String currentSubTableId = param.get("currentSubTableId",defaultTabId);
		if (!currentSubTableId.equals(MasterSubService.BASE_TABLE_ID)){
			String subRecordsKey = currentSubTableId + "Records";
			if (!this.getAttributesContainer().containsKey(subRecordsKey)){
				List<DataRow> subRecords = getService().findSubRecords(currentSubTableId, param);
				this.setAttribute(currentSubTableId+"Records", subRecords);
			}
		}
		this.setAttribute("currentSubTableId", currentSubTableId);
		this.setAttribute("currentSubTableIndex", getTabIndex(currentSubTableId));
		String operateType = param.get(OperaType.KEY);
		this.setOperaType(operateType);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
		setAttribute("ORG_STATE", FormSelectFactory.create("PER_STATE")
				.addSelectedValue(getOperaAttributeValue("ORG_STATE", "0")));
		setAttribute("ORG_TYPE", FormSelectFactory.create("ORG_TYPE")
				.addSelectedValue(getOperaAttributeValue("ORG_TYPE", "")));
		setAttribute("ORG_CLASSIFICATION", FormSelectFactory.create("ORG_CLASSIFICATION")
				.addSelectedValue(getOperaAttributeValue("ORG_CLASSIFICATION", "")));
		setAttribute("ORG_SOURCES", FormSelectFactory.create("ORG_SOURCES")
				.addSelectedValue(getOperaAttributeValue("ORG_SOURCES", "")));
		
        initMappingItem("PROCUST_VISIT_EFFECT",
                FormSelectFactory.create("VISIT_EFFECT").getContent());
		initMappingItem("PROCUST_VISIT_TYPE",
		        FormSelectFactory.create("VISIT_TYPE").getContent());
    }
    
    public ViewRenderer doNoFollowUpAction(DataParam param){
		param.put("TASK_FOLLOW_STATE", "HaveFollowUp");
		getService().updateTaskStateRecord(param);
		param.put("ORG_STATE", "1");
		getService().updateTaskOrgStateRecord(param);
    	return new RedirectRenderer(getHandlerURL(listHandlerClass)+"&TASK_REVIEW_ID="+param.get("TASK_REVIEW_ID"));
    }
    
    public ViewRenderer doGoBackAction(DataParam param){
    	return new RedirectRenderer(getHandlerURL(listHandlerClass)+"&TASK_REVIEW_ID="+param.get("TASK_REVIEW_ID"));
    }

    protected String[] getEntryEditFields(String currentSubTableId) {
        List<String> temp = new ArrayList<String>();

        return temp.toArray(new String[] {  });
    }

    protected String getEntryEditTablePK(String currentSubTableId) {
        HashMap<String, String> primaryKeys = new HashMap<String, String>();
        primaryKeys.put("ProCustVisitInfo", "ORG_ID");

        return primaryKeys.get(currentSubTableId);
    }

    protected String getEntryEditForeignKey(String currentSubTableId) {
        HashMap<String, String> foreignKeys = new HashMap<String, String>();
        foreignKeys.put("ProCustVisitInfo", "ORG_ID");

        return foreignKeys.get(currentSubTableId);
    }

    protected ColdCallsManage getService() {
        return (ColdCallsManage) this.lookupService(this.getServiceId());
    }
}
