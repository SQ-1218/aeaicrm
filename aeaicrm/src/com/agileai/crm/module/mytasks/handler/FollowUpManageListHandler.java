package com.agileai.crm.module.mytasks.handler;

import java.util.Date;
import java.util.List;

import com.agileai.common.KeyGenerator;
import com.agileai.crm.cxmodule.FollowUpManage;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.MasterSubListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;

public class FollowUpManageListHandler
        extends MasterSubListHandler {
	protected String rootId = "00000000-0000-0000-00000000000000000";
    public FollowUpManageListHandler() {
        super();
        this.editHandlerClazz = FollowUpManageEditHandler.class;
        this.serviceId = buildServiceId(FollowUpManage.class);
    }
    
	public ViewRenderer prepareDisplay(DataParam param){
		if(!rootId.equals(param.get("TASK_REVIEW_ID"))){
			DataRow dataRow = getService().getTaskReviewRecord(param);
			String taskReviewState = dataRow.getString("TASK_REVIEW_STATE");
			this.setAttribute("TASK_REVIEW_STATE", taskReviewState);
		}
		User user = (User) getUser();
		param.put("userId", user.getUserId());
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().findMasterRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}

    protected void processPageAttributes(DataParam param) {
        initMappingItem("CUST_INDUSTRY",
                FormSelectFactory.create("CUST_INDUSTRY")
                                 .getContent());
        initMappingItem("CUST_SCALE",
                FormSelectFactory.create("CUST_SCALE").getContent());
        initMappingItem("CUST_NATURE",
                FormSelectFactory.create("CUST_NATURE").getContent());
        initMappingItem("CUST_STATE",
                FormSelectFactory.create("CUST_STATE").getContent());
		initMappingItem("TASK_FOLLOW_STATE", 
				FormSelectFactory.create("TASK_FOLLOW_STATUS").getContent());
    }

    protected void initParameters(DataParam param) {
    }
    
    public ViewRenderer doCreateTaskAction(DataParam param) {
		String ids = param.get("ids");
    	String[] idArray = ids.split(",");
    	User user = (User) getUser();
    	String userId = user.getUserId();
    	String dateStr = DateUtil.getDateByType(DateUtil.YYMMDDHHMISS_HORIZONTAL, new Date());
    	String taskReviewState = param.get("TASK_REVIEW_STATE");
    	String taskFollowState = "";
    	if("Init".equals(taskReviewState)){
    		taskFollowState = "NoFollowUp";
    	}else{
    		taskFollowState = "Init";
    	}
    	for(int i=0;i < idArray.length;i++){
    		String custId = idArray[i];
    		DataRow dataRow = getService().getMyCustRecord(new DataParam("custId",custId));
    		DataParam myTasksParam = new DataParam();
    		myTasksParam.put("TASK_ID", KeyGenerator.instance().genKey());
    		myTasksParam.put("ORG_ID", dataRow.get("ORG_ID"));
    		myTasksParam.put("CUST_ID", custId);
    		myTasksParam.put("TASK_REVIEW_ID", param.get("TASK_REVIEW_ID"));
    		myTasksParam.put("SALE_ID", userId);
    		myTasksParam.put("TASK_FOLLOW_STATE", taskFollowState);
    		myTasksParam.put("TASK_CLASS", "FollowUp");
    		myTasksParam.put("TASK_CREATE_TIME", dateStr);
    		myTasksParam.put("TASK_FINISH_TIME", "");
    		myTasksParam.put("TASK_CUST_STATE", "");
    		getService().createMasterRecord(myTasksParam);
    	}
    	return prepareDisplay(param);
    }
    
	public ViewRenderer doDeleteAction(DataParam param){
		storeParam(param);
		getService().deleteClusterRecords(param);	
		return prepareDisplay(param);
	}

    protected FollowUpManage getService() {
        return (FollowUpManage) this.lookupService(this.getServiceId());
    }
}
