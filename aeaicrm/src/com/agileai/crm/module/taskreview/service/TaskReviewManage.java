package com.agileai.crm.module.taskreview.service;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface TaskReviewManage
        extends StandardService {
	DataRow retrieveCurrentWeekRow();
	DataRow getWeekRow(String weekId);
	
	DataRow getBeforeWeekRow(String tcId);
	DataRow getNextWeekRow(String tcId);
	
	List<DataRow> querySaleRecords();
	
}
